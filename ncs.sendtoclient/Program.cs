﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;

namespace ncs.sendtoclient
{
    class Program
    {
        static void Main(string[] args)
        {
            FileStream stream1 = new FileStream("error.txt", FileMode.OpenOrCreate);
            StreamWriter writer1 = new StreamWriter(stream1, Encoding.UTF8);

          
            EventLog myLog = new EventLog();
            myLog.Source = "Aplication";
            try
            {
                if (args.Length < 7)
                    throw new Exception("number of invalid arguments");

                string rpttyp = args[0];
                string server = args[1];
                string username = args[2];
                string password = args[3];
                string remotepath = args[4];
                string pathOutbound = args[5];
                string uploadfiles = args[6];
                string pathLog = string.Empty;
                if (args.Length >= 8)
                {
                    pathLog = args[7];
                }

                if (!Directory.Exists(pathOutbound))
                    throw new Exception("Directory Outbound no exists. " + pathOutbound);

                string filelog = string.Format("{0}\\{1}.txt", pathOutbound, rpttyp);

                FileStream stream = new FileStream(filelog, FileMode.Create);
                StreamWriter writer = new StreamWriter(stream, Encoding.UTF8);
                
                string[] files = Directory.GetFiles(pathOutbound, uploadfiles);
                ftp obj = new ftp(server, username, password, remotepath);

                string fileFrom = string.Empty;
                foreach (string file in files)
                {
                    bool status = obj.Sends(pathOutbound, file);
                    if (status == true)
                    {
                        if (pathLog.Equals(string.Empty))
                            Directory.Delete(file);
                        else
                        {
                            fileFrom = string.Format("{0}\\{1}", pathLog, Path.GetFileName(file));
                            Directory.Move(file, fileFrom);
                        }
                    }
                    else
                    {
                        myLog.WriteEntry(string.Format("{0} error. it was not successfully uploaded.", Path.GetFileName(file)), EventLogEntryType.Error, 74);
                    }
                    string newline = string.Format("{0}{1}", new object[] { Path.GetFileName(file).PadRight(100), (status == true ? "OK" : "ER") });
                    writer.WriteLine(newline);
                }
                writer.Flush();
                writer.Close();
                stream.Close();

                writer = null;
                stream = null;
                ///
            }
            catch (Exception eEx)
            {
                writer1.WriteLine(string.Format("{0}\n{1}", eEx.Message, eEx.StackTrace));
                myLog.WriteEntry(string.Format("{0}\n{1}", eEx.Message, eEx.StackTrace), EventLogEntryType.Error, 89);
            }
            finally
            {
                writer1.Flush();
                writer1.Close();

                stream1.Close();
                writer1 = null;
                stream1 = null;
            }
        }
    }
}
