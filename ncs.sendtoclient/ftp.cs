﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;

namespace ncs.sendtoclient
{
    public class ftp
    {
        private FtpWebRequest request;
        private string message;
        public string getMessage { get { return this.message;  } }
        private string server;
        private string username;
        private string password;
        private string remotepath;
        public ftp(string server, string username, string password, string remotepath)
        {
            this.server = server;
            this.username = username;
            this.password = password;
            this.remotepath = remotepath;
        }
        public bool Sends(string path, string file)
        {
            bool status = false;
            this.message = string.Empty;
            try
            {
                string remotepath = string.Format("ftp://{0}/{1}/{2}",new object[] {this.server, this.remotepath, Path.GetFileName(file) });
                // Get the object used to communicate with the server.

                request = (FtpWebRequest)WebRequest.Create(remotepath);
                request.KeepAlive = true;
                request.Timeout = 10 * 60 * 1000;//10 minus
                // request.UsePassive = false;
                request.Method = WebRequestMethods.Ftp.UploadFile;

                // This example assumes the FTP site uses anonymous logon.
                request.Credentials = new NetworkCredential(this.username, this.password);

                // Copy the contents of the file to the request stream.
                byte[] fileContents;
                using (StreamReader sourceStream = new StreamReader(file))
                {
                    fileContents = Encoding.UTF8.GetBytes(sourceStream.ReadToEnd());
                }

                request.ContentLength = fileContents.Length;

                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(fileContents, 0, fileContents.Length);
                }

                using (FtpWebResponse response = (FtpWebResponse)request.GetResponse())
                {
                    Console.WriteLine($"Upload File Complete, status {response.StatusDescription}");
                }
                status = true;
            }
            catch (Exception eEX)
            {
                this.message = string.Format("ERROR: {0}\n{1}", Path.GetFileName(file), eEX.Message);
                status = false;
            }
            finally
            {

            }
            return status;
        }
    }
}
